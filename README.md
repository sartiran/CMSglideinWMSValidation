# CMSglideinWMSValidation
Validation scripts for CMS glideins

# CMSLPCMapper

In order to use the LPC mapper, you want to add the following line to the configuration:

JOB_ROUTER.CLASSAD_USER_PYTHON_MODULES = $(CLASSAD_USER_PYTHON_MODULES), CMSLPCMapper

By default, it will read from the URL specified by the HTCondor config knob CMSLPC_USER_URL.

If that is not set (or an error occurs), it will read from CMSLPC_USER_CACHE.  CMSLPC_USER_CACHE
defaults to $(SPOOL)/cmslpc_cache.txt

# Pilot Startup Site Test (PSST)

PSST is a set of validation scripts, created by the Site Support team, and
mantained by Rokas. More information available here:

https://twiki.cern.ch/twiki/bin/view/CMSPublic/PilotStartupSiteTest

This validation scripts report their results to Elastic Search and for this
a password is required. For security reasons, this password should not be
stored in the git repository, so this has to be manually included by the
GlideinWMS Frontend operator after the repository is cloned for the first time(1).
To prevent this file to be overwritten, a git command is executed to
instruct git to ignore changes on that specific file(2), this is only requiered the
very first time the repository is cloned.

(1) At the moment we are looking for a bettwer option that allows us to avoid 
    this manual procedure.

(2) git update-index --assume-unchaged reporting/es_report.py

Once the password is in place a "make" is done to archive/compress all the
validation scripts in a single tar.gz file (psst.tgz). This is the file handed
to every glidein. Every time something gets changed witihn the PSST repository
a "make" would be needed to update the psst.tgz file (This might be automated
with puppet, but haven't got there yet.).

The psst.tgz file is included in a .gitignore file so that git will not see this
file as "untracked".

The password could be obtained either from the HA Frontend or directly from the
Site Support team.


## The Submodule

The PSST project used to be a folder inside the CMSglideinWMSValidation
repository. Following Brian's suggestion[1], it was moved to a separated
repository[2] and added as a submodule in CMSglideinWMSValidation.


### Puppet.

From the puppet point of view, adding this as a submodule, does not change
anything. When puppet clones the parent repository CMSglideinWMSValidation, it
will also fetch recursively all the submodules contained on it.
This is the default behavior of puppet. To prevent puppet from fetching the
submodules, it is necessary to add the attribute "submodule => false" in the
vcsrepo definition[3].

### When cloning manually

When cloning manually, one must execute the following command to get the content
of the submodule cloned in the submodule folder. This has to be executud
in the parent repository directory.

     git submodule update --init --recursive 

NOTE: when te cloning is done by puppet there is no need to execute the previous
command, puppet will take car of it.




### Git.

From the GIT perspective, any change done to the PSST repository will not be
fetched by our local copy unless it is explicitly set on the parent
repository(CMSglideinWMSValidation). This is done by going inside the folder
containing the submodule and pull/checkout wahtever branch/revision you want.
Once you are in the desired revision of the submodule, go back to the parent
repository and do a commit.

e.g.

    cd PSST
    
    git checkout BRANCH_NAME
    
    git pull -> Get the latest version
    
    git checkout COMMIT_HASH -> go to the desired revision (optional)
    
    cd ../ -> go to parent repository
    
    git commit -am "updating submodule to new revision"
    
    git push origin BRANCH_NAME



[1] https://gitlab.cern.ch/CMSSI/CMSglideinWMSValidation/issues/27

[2] https://gitlab.cern.ch/rmaciula/PSST

[3] https://forge.puppet.com/puppetlabs/vcsrepo/readme#git

